package com.devcamp.countryapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryapi.model.CCountry;
import com.devcamp.countryapi.repository.ICountryRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CountryController {
    @Autowired
    ICountryRepository countryRepository;

    @GetMapping("/countries")
    public List<CCountry> getAllCountry() {
        return countryRepository.findAll();
    }

    @GetMapping("/countries/{id}")
    public CCountry getCountriesById(@PathVariable("id") long id) {
        return countryRepository.findById(id).get();

    }

    @PostMapping("/countries")
    public ResponseEntity<CCountry> createCountry(@RequestBody CCountry pCountry) {
        try {
            CCountry cCountry = new CCountry();
            cCountry.setCountryCode(pCountry.getCountryCode());
            cCountry.setCountryName(pCountry.getCountryName());
            cCountry.setRegions(pCountry.getRegions());
            return new ResponseEntity<CCountry>(countryRepository.save(cCountry), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/countries/{id}")
    public ResponseEntity<CCountry> updateCountry(@PathVariable("id") long id, @RequestBody CCountry pCCountry) {
        Optional<CCountry> countryData = countryRepository.findById(id);
        if (countryData.isPresent()) {
            countryData.get().setCountryCode(pCCountry.getCountryCode());
            countryData.get().setCountryName(pCCountry.getCountryName());
            countryData.get().setRegions(pCCountry.getRegions());
            return new ResponseEntity<>(countryRepository.save(countryData.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/countries/{id}")
    public ResponseEntity<CCountry> deleteCountry(@PathVariable("id") long id) {
        countryRepository.deleteById(id);
        return new ResponseEntity<CCountry>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/country-count")
    public long countryCount() {
        return countryRepository.count();
    }

    @GetMapping("/country/check/{id}")
    public boolean checkCOuntryById(@PathVariable("id") long id) {
        return countryRepository.existsById(id);
    }

}
